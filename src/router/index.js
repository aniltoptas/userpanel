import Vue from "vue";
import VueRouter from "vue-router";
import Register from "../views/Register.vue";
import Login from "../views/Login.vue";
import Edit from '../views/Edit.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    redirect: '/register'
  },
  {
    path: '/register',
    name: 'register',
    component: Register
  },
  {
    path: "/login",
    name: "login",
    component: Login
  },
  {
    path: '/edit',
    name: 'edit',
    component: Edit
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
